"use strict";

// Import library
const bodyParser = require("body-parser");
const express = require("express");
const session = require("express-session");
const lti = require("./lti");
require("dotenv").config();
const requestP = require("request-promise");

const port = process.env.PORT || 8080;
// this express server should be secured/hardened for production use
const app = express();

// memory store shouldn't be used in production
app.use(
  session({
    secret: process.env.SESSION_SECRET || "dev",
    resave: false,
    saveUninitialized: true,
  })
);
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

app.get("/issue", (req, res, next) => {
  if (req.session.userId) {
    let access_key = req.session.access_key;
    let secret_key = req.session.secret_key;
    try {
      var options = {
        method: method,
        uri: "https://dev-app-api.certif-id.com/api/public/identity/v1/entityaccess/token",
        headers: "",
        body: {key: access_key, secret: secret_key},
        json: true,
        resolveWithFullResponse: true,
      };
      var authResponse = requestP(options);
      resolve(authResponse);
      var options = {
        method: method,
        uri: "https://dev-app-api.certif-id.com/api/certification/v1/quickIssuance/recipients/data/upload",
        headers: {Authorization: `Bearer ${authResponse.payload.accessToken}`},
        body: {
          course_uuid: "dde70c92-a678-4beb-b30f-41ac19e6a74c",
          template_uuid: "cb45f553-f59a-11ee-9fa6-06b8d3a3a95a",
          is_send_certification_email: false,
          is_attach_certificate: false,
          is_send_certificate_public_link: true,
          student: [
            {
              "First Name": "Umesh",
              Email: "umesh.jangid@smartsensesolutions.com",
              "Primary Value": "Email",
            },
          ],
        },
        json: true,
        resolveWithFullResponse: true,
      };
      var authResponse = requestP(options);
      resolve(authResponse);
    } catch (err) {}
  } else {
    next(
      new Error(
        "Session invalid. Please login via LTI to use this application."
      )
    );
  }
});

app.post("/launch_lti", lti.handleLaunch);

app.listen(port, () =>
  console.log(`Certif-id B2B Lti integration app listening on port ${port}!`)
);
